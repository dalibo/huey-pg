# To run consumer:
#
#     huey_consumer -w 2 -n huey_pg.huey
#
# To send tasks:
#
#     python example.py
#

import logging
import pdb
import os
import sys
from time import sleep

import psycopg2.pool
from huey_pg import PostgresHuey


logger = logging.getLogger(__name__)
pool = psycopg2.pool.ThreadedConnectionPool(0, 4, "")
huey = PostgresHuey(connection_pool=pool)


@huey.task()
def sleeper(seconds):
    sleep(seconds)


# Overwrite __main__ module name.
sleeper.task_class.__module__ = 'huey_pg'


def main():
    for _ in range(10):
        res = sleeper(2)
        logger.info("Queued %s.", res.task)


if '__main__' == __name__:
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(levelname)1.1s: %(message)s',
    )

    try:
        exit(main())
    except (pdb.bdb.BdbQuit, KeyboardInterrupt):
        logger.info("Interrupted.")
    except Exception:
        logger.exception('Unhandled error:')
        if sys.stdout.isatty():
            logger.debug("Dropping in debugger.")
            pdb.post_mortem(sys.exc_info()[2])

    exit(os.EX_SOFTWARE)
